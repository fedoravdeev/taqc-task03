package org.fedoravdeev.triangle;

import java.util.*;

import static java.lang.Math.*;

public class Triangle {

    public void run() {
        String answer;
        HashMap<String, Float> map = new HashMap<String, Float>();

        System.out.println("--\nProgram sort by square triangle. Please input parameters (comma separated): name, a->b, b->c, c->a:");
        // TO DO
        // add file scanner
        // useDelimiter ??
        Scanner in = new Scanner(System.in);
        do {
            System.out.print("Input name triangle and sides:");
            String triangle = in.next();
            checkTriangle(triangle, map);
            // TO DO
            // check triangle
            in.nextLine();

            System.out.print("--\nDo you want continue: y/n (yes/no)");
            answer = in.next();
            System.out.printf("Your answer: %s \n", answer);
        } while (answer.equalsIgnoreCase("y") || answer.equalsIgnoreCase("yes"));
        in.close();

        if (!map.isEmpty()) {
            printSortTriangle(map);
        }
    }

    private void printSortTriangle(HashMap<String,Float> map) {
        List<Map.Entry<String, Float>> toSort = new ArrayList<>();
        for (Map.Entry<String, Float> stringFloatEntry : map.entrySet()) {
            toSort.add(stringFloatEntry);
        }
        toSort.sort(Map.Entry.<String, Float>comparingByValue().reversed());
        System.out.println("============= Triangles list: ===============");
        int i = 1;
        for (Map.Entry<String, Float> stringFloatEntry : toSort) {
            System.out.println(i++ + ".[Triangle " + stringFloatEntry.getKey() + "]: " + stringFloatEntry.getValue() + " cm");
        }
    }

    private void checkTriangle(String triangle, HashMap<String, Float> map) {
        String[] s = triangle.split(",");
//        for (String s1: s) {
//            System.out.println(s1);
//        }
        if (s.length == 4) {
            String name = s[0];
            float ab = Float.valueOf(s[1].replace(" ",""));
            float bc = Float.valueOf(s[2].replace(" ",""));
            float ca = Float.valueOf(s[3].replace(" ",""));
            float pp = (ab + bc + ca) / 2;
            if ((ab + bc <= ca) || (ab + ca <= bc) || (ca + bc <= ab)) {
                System.out.println("Incorrecr side triangle");
                return;
            }
            float sq = (float) pow(pp * (pp - ab) * (pp - bc) * (pp - ca), 0.5);
            map.put(name, sq);
            return;
       }
        System.out.println("Could you please input correct parameters.");
    }
}
